
const config = require('../config.json');
const jwt = require('jsonwebtoken');
Movie = require('../model/movieModel');

exports.createmovie = function (req, res,next) {

    var movie = new Movie();
    movie.movie_name = req.body.movie_name ? req.body.movie_name : '';
    movie.img = req.body.img ? req.body.img : '';

     // save the contact and check for errors
    Movie.find({movie_name:movie.movie_name}, function (errs, docs) {
        if (docs.length){
            res.status(409).json({
                message: 'Movie already exists',
            });
        
        }else{

            movie.save(function (err) {
                // Check for validation error
                if (err)
                    res.json(err);
                else
                    res.json({
                        message: 'New Movie created!',
                        data: movie
                    });
            });
            
        }
        
    });
    
    
}
exports.index = function (req, res) {
    Movie.find({},function (err, movies) {
        if (err) {
            res.json({
                status: "error",
                message: err,
            });
        }
        res.json({
            status: "success",
            message: "Movies retrieved successfully",
            data: movies
        });
    });
};

exports.getById = function (req, res) {
    Movie.findById({_id:req.params.movie_id}, function (err, movie) {
        if (err)
           res.send(err);
        else{
            res.json({
                status: "success",
                message: "Movies retrieved successfully",
                data: movie
            });
        }
    });
}

// Handle delete Movie
exports.delete = function (req, res) {
    Movie.remove({
        _id: req.params.movie_id
    }, function (err, movie) {
        if (err)
            res.send(err);
        res.json({
            status: "success",
            message: 'Movie deleted'
        });
    });
};

// exports.moviereport = function (req, res) {
//     Movie.aggregate([
//         {
//             $lookup: {
//                from: "bookings",
//                localField: "ObjectId(_id)",
//                foreignField: "ObjectId(movie_id)",
//                as: "movie_data"
//             },
            
//         },
        
//         { $group : { _id : "$movie_name",count: { $sum: 1 } } }],function (err, show) {
//         if (err) {
//             res.json({
//                 status: "error",
//                 message: err,
//             });
//         }
//         res.json({
//             status: "success",
//             message: "Booking retrieved successfully",
//             data: show
//         });
//     });
// };