
Show = require('../model/showModel');

exports.createshow = function (req, res, next) {

    var show = new Show();
    show.movie_id = req.body.movie_id ? req.body.movie_id : '';
    show.showdate = req.body.showdate ? req.body.showdate : '';
    show.showtime = req.body.showtime ? req.body.showtime : '';
    show.status = req.body.status ? req.body.status : '';

    Show.find({ showdate: show.showdate, showtime: show.showtime }, function (errs, docs) {
        if (docs.length) {
            res.status(409).json({
                message: 'Show time already scheduled',
            });

        } else {

            show.save(function (err) {
                if (err)
                    res.json(err);
                else
                    res.json({
                        message: 'New Show created!',
                        data: show
                    });
            });


        }

    });


}

exports.getAllShows = function (req, res) {
    Show.aggregate([{
        $lookup: {
            from: "movies",
            localField: "movie_id",
            foreignField: "_id",
            as: "moviedata"
        }
    },
    {
        $project: {
            _id: 1,
            showtime: 1,
            showdate: 1,
            status: 1,
            "moviedata._id": 1,
            "moviedata.movie_name": 1,
        },
    },
    ], function (err, show) {
        if (err) {
            res.json({
                status: "error",
                message: err,
            });
        }
        res.json({
            status: "success",
            message: "Shows retrieved successfully",
            data: show
        });
    });
};

exports.getByMovieId = function (req, res) {
    var date = req.params.date ? req.params.date : new Date();
    Show.find({ movie_id: req.params.movie_id, showdate: date, status: "Scheduled" }, function (err, show) {
        if (err) {
            res.json({
                status: "error",
                message: err,
            });
        }
        res.json({
            status: "success",
            message: "Shows retrieved successfully",
            data: show
        });
    });
};

// Handle delete Show
exports.delete = function (req, res) {
    Show.remove({
        _id: req.params.show_id
    }, function (err, show) {
        if (err)
            res.send(err);
        res.json({
            status: "success",
            message: 'Show deleted'
        });
    });
};

exports.showreport = function (req, res) {
    Show.aggregate([
        {
            $lookup: {
                from: "bookings",
                localField: "ObjectId(_id)",
                foreignField: "ObjectId(show_id)",
                as: "show_data"
            },

        },
        { $group: { _id: "$showtime", count: { $sum: 1 } } }], function (err, show) {
            if (err) {
                res.json({
                    status: "error",
                    message: err,
                });
            }
            res.json({
                status: "success",
                message: "Booking retrieved successfully",
                data: show
            });
        });
};

