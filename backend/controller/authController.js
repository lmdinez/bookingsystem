
const config = require('../config.json');
const jwt = require('jsonwebtoken');
User = require('../model/userModel');

exports.authenticate = function (req, res,next) {

    let username = req.body.username ? req.body.username : '';
    let password = req.body.password ? req.body.password : '';
    let role = req.body.role ? req.body.role : '';


    User.find({$and:[{username: username},{password:password},{role:role}]}, function(err, user) {
        if(!user || user.length <1){
            res.status(401).json({
                status: "failed",
                message: "Invalid credentials"

            });
        }else{
            
            const token = jwt.sign({ sub: user.id }, config.secret, { expiresIn: '7d' });
            
            res.json({
                status: "success",
                message: "User logged in successfully",
                token: token,
                "user": user
            });
        }
        
        
    });
    
}

exports.createuser = function (req, res,next) {
    
    let user = new User()
    user.username = req.body.username ? req.body.username : ''; 
    user.password = req.body.password ? req.body.password : '';
    user.name = req.body.name ? req.body.name : ''; 
    user.email = req.body.email ? req.body.email : '';
    user.phone = req.body.phone ? req.body.phone : '';

    User.find({$or:[{username: user.username},{email:user.email}]}, function(err, userdata) {
        console.log(userdata)
        if(userdata.length >0){
            res.status(400).json({
                status: "failed",
                message: "User already exists"

            });
        }else{
            user.save(function (err) {
                if (err)
                    res.json(err);
                else
                    res.json({
                        message: 'New user created!',
                        data: user
                    });
                });
        }    
    
    });
    
}

// Handle update user info
exports.update = function (req, res) {
    User.findById(req.params.user_id, function (err, user) {
        if (err)
            res.send(err);
        user.username = req.body.username ? req.body.username : user.username;
        user.password = req.body.password ? req.body.password : user.password;
        user.firstname = req.body.firstname ? req.body.firstname : user.firstname;
        user.lastname = req.body.lastname ? req.body.lastname : user.lastname;
        user.email = req.body.email;
        user.phone = req.body.phone;
        // save the user and check for errors
        user.save(function (err) {
            if (err)
                res.json(err);
            res.json({
                message: 'User Info updated',
                data: user
            });
        });
    });
};

// Handle delete user
exports.delete = function (req, res) {
    User.remove({
        _id: req.params.user_id
    }, function (err, contact) {
        if (err)
            res.send(err);
        res.json({
            status: "success",
            message: 'User deleted'
        });
    });
};