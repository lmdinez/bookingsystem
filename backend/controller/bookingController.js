Booking = require('../model/bookingModel');
var mongoose = require('mongoose');
exports.booking = function (req, res) {
    var booking = new Booking();
    booking.show_id = req.body.show_id ? req.body.show_id : '';
    booking.movie_id = req.body.movie_id ? req.body.movie_id : '';
    booking.consumer_id = req.body.consumer_id ? req.body.consumer_id : '';
    booking.number_of_tickets = req.body.number_of_tickets ? req.body.number_of_tickets : '';
    booking.status = req.body.status ? req.body.status : '';
    booking.total_amount = req.body.total_amount ? req.body.total_amount : '';

    // save the contact and check for errors
    booking.save(function (err) {
        // Check for validation error
        if (err)
            res.json(err);
        else
            res.json({
                message: 'Ticket Booked!',
                data: booking
            });
    });
};

exports.bookingupdate = function (req, res) {
    Booking.findById(req.params.booking_id, function (err, booking) {
        if (err)
            res.send(err);
        booking.status = req.body.status ? req.body.status : booking.status;
        booking.number_of_tickets = req.body.number_of_tickets ? req.body.number_of_tickets : '';

        // save the contact and check for errors
        booking.save(function (err) {
            if (err)
                res.json(err);
            res.json({
                message: 'Booking Info updated',
                data: booking
            });
        });
    });
};

exports.index = function (req, res) {
    Booking.aggregate([
        {
            $lookup: {
                from: "movies",
                localField: "movie_id",
                foreignField: "_id",
                as: "movie"
            },

        },
        {
            $lookup: {
                from: "users",
                localField: "consumer_id",
                foreignField: "_id",
                as: "userdata"
            },
        },
        {
            $lookup: {
                from: "shows",
                localField: "show_id",
                foreignField: "_id",
                as: "show"
            },

        },
        {
            $project: {
                _id: 1,
                number_of_tickets: 1,
                status: 1,
                total_amount: 1,
                "movie._id": 1,
                "movie.movie_name": 1,
                "show.showtime": 1,
                "show.showdate": 1,
                "show._id": 1,
                "userdata.username": 1,
                "userdata.name": 1,
                "userdata._id": 1
            },
        },
    ], function (err, booking) {
        if (err)
            res.send(err);
        else {
            res.json({
                status: "success",
                message: "Booking retrieved successfully",
                data: booking
            });
        }
    });
};

// Handle delete Show
exports.delete = function (req, res) {
    Booking.remove({
        _id: req.params.booking_id
    }, function (err, booking) {
        if (err)
            res.send(err);
        res.json({
            status: "success",
            message: 'Booking deleted'
        });
    });
};


exports.bookingreport = function (req, res) {
    Booking.aggregate([
        {
            $group: {
                _id: "$show_id",
                lastData: {
                    $last: '$number_of_tickets'
                },
                count: { $sum: { $multiply: ["$number_of_tickets", 1] } },
            }
        },
        {
            $project: {
                number_of_tickets: '$lastData'
            }
        }

    ], function (err, booking) {
        if (err) {
            res.json({
                status: "error",
                message: err,
            });
        }
        res.json({
            status: "success",
            message: "Booking retrieved successfully",
            data: booking
        });
    });
};

exports.getByConsumerId = function (req, res) {
    Booking
        .aggregate([
            {
                $match: {
                    consumer_id: mongoose.Types.ObjectId(req.params.consumer_id)
                }
            },
            {
                $lookup: {
                    from: "movies",
                    localField: "movie_id",
                    foreignField: "_id",
                    as: "movie"
                },

            },
            {
                $lookup: {
                    from: "users",
                    localField: "consumer_id",
                    foreignField: "_id",
                    as: "userdata"
                },
            },
            {
                $lookup: {
                    from: "shows",
                    localField: "show_id",
                    foreignField: "_id",
                    as: "show"
                },

            },

            {
                $project: {
                    _id: 1,
                    number_of_tickets: 1,
                    status: 1,
                    total_amount: 1,
                    "movie._id": 1,
                    "movie.movie_name": 1,
                    "show.showtime": 1,
                    "show.showdate": 1,
                    "show._id": 1,
                    "userdata.name":1
                },
            },
        ], function (err, booking) {
            if (err)
                res.send(err);
            else {
                res.json({
                    status: "success",
                    message: "Booking retrieved successfully",
                    data: booking
                });
            }
        });
}