let express = require('express');
let bodyParser = require('body-parser');
let mongoose = require('mongoose');

const jwt = require('./helpers/jwt');
const errorHandler = require('./helpers/error-handler');
var cors = require('cors')

let app = express();
app.use(jwt());
app.use(errorHandler);
const morgan = require("morgan")
app.use(morgan("dev"));




let apiRoutes = require("./routes/api-routes");
app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(bodyParser.json());
const uri = "mongodb+srv://root:ZoMD5UT4R5rWjFOC@cluster0.ufivd.mongodb.net/ticket_bookingcc?retryWrites=true&w=majority";
mongoose.connect(uri, { useNewUrlParser: true, useUnifiedTopology: true });


var db = mongoose.connection;


if (!db)
  console.log("Error connecting db")
else
  console.log("Db connected successfully")

var port = process.env.PORT || 8080;

app.get('/', (req, res) => res.send('Hello World with Express'));

app.use('/api', cors(), apiRoutes);
app.use(cors());
app.listen(port, function () {
  console.log("Running api on port " + port);
});