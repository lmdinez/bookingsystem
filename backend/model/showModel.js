// movieModel.js
var mongoose = require('mongoose');
// Setup schema
var showSchema = mongoose.Schema({
    // movie_id: {
    //     type: mongoose.Schema.Types.ObjectId,
    //     required: true,
    //     ref: 'movie'
    // },
    movie_id: { type: mongoose.Schema.Types.ObjectId, ref: 'Movie' },
    showtime: {
        type: String,
        required: true
    },
    showdate: {
        type: Date,
        required: true
    },
    status: {
        type: String,
        required: true
    },
    created_date: {
        type: Date,
        default: Date.now
    }
});
// Export User model
var Show = module.exports = mongoose.model('show', showSchema);
module.exports.get = function (callback, limit) {
    Show.find(callback).limit(limit);
}