// contactModel.js
var mongoose = require('mongoose');
// Setup schema
var bookingSchema = mongoose.Schema({
    movie_id: { type: mongoose.Schema.Types.ObjectId, ref: 'Movie' },
    consumer_id: {
        type: mongoose.Schema.Types.ObjectId, ref: 'User' 
    },
    show_id: {
        type: mongoose.Schema.Types.ObjectId, ref: 'Show' 
    },
    number_of_tickets: Number,
    status: String,
    create_date: {
        type: Date,
        default: Date.now
    },
    total_amount:{
        type: Number,
        required: true
    }
});
// Export Booking model
var Booking = module.exports = mongoose.model('booking', bookingSchema);
module.exports.get = function (callback, limit) {
    Booking.find(callback).limit(limit);
}