var mongoose = require('mongoose');
var movieSchema = mongoose.Schema({
    movie_name: {
        type: String,
        required: true
    },
    img:
    {
        data: Buffer,
        contentType: String
    },
    created_date: {
        type: Date,
        default: Date.now
    }
});
var Movie = module.exports = mongoose.model('movie', movieSchema);
module.exports.get = function (callback, limit) {
    Movie.find(callback).limit(limit);
}