
let router = require('express').Router();
// Set default API response
router.get('/', function (req, res) {
    res.json({
        status: 'API Its Working',
        message: '',
    });
});
// Import  controller
var authController = require('../controller/authController');
var showController = require('../controller/showController');
var movieController = require('../controller/movieController');
var bookingController = require('../controller/bookingController');

// routes
router.route('/auth')
    .post(authController.authenticate);

router.route('/createuser')
    .post(authController.createuser);

router.route('/show')
    .post(showController.createshow)
    .get(showController.getAllShows);

router.route('/show/:show_id')
    .delete(showController.delete);
    

router.route('/show/:movie_id/:date')
    .get(showController.getByMovieId);

router.route('/movie')
    .post(movieController.createmovie)
    .get(movieController.index);

router.route('/movie/:movie_id')
    .get(movieController.getById)
    .delete(movieController.delete);

router.route('/booking')
    .post(bookingController.booking)
    .get(bookingController.index);

router.route('/showreport')
    .get(showController.showreport);

// router.route('/moviereport')
//     .get(movieController.moviereport);

router.route('/bookingreport')
    .get(bookingController.bookingreport);

router.route('/booking/:booking_id')
    .put(bookingController.bookingupdate);

router.route('/booking/:consumer_id')
    .get(bookingController.getByConsumerId);



// Export API routes
module.exports = router;